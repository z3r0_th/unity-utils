﻿using System.Collections;
using UnityEngine;

namespace UnityUtils
{
    public static class Util
    {
        public static T[] ConvertToArray<T>(IList list)
        {
            T[] ret = new T[list.Count];
            list.CopyTo(ret, 0);
            return ret;
        }
    }
}
