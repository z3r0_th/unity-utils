﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

namespace UnityUtils.UI
{
    public class LayoutForceUpdate : MonoBehaviour
    {
        [SerializeField] private int interactions = 3;

        private RectTransform rectTransform;

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
        }

        IEnumerator Start()
        {
            for (int i = 0; i < interactions; ++i)
            {
                yield return new WaitForEndOfFrame();
                LayoutRebuilder.ForceRebuildLayoutImmediate(rectTransform);
            }
        }

        private void OnEnable()
        {
            LayoutRebuilder.MarkLayoutForRebuild(rectTransform);
        }

        public void UpdateLayout()
        {
            LayoutRebuilder.MarkLayoutForRebuild(rectTransform);
        }
    }
}