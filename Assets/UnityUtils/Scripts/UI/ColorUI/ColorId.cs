﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityUtils.UI
{
    [CreateAssetMenu(fileName = "colorId", menuName = "UnityUtils/Color/Id")]
    public class ColorId : ScriptableObject
    {
    }
}