﻿using UnityEngine;

namespace UnityUtils.UI
{
    [CreateAssetMenu(fileName = "color", menuName = "UnityUtils/Color/UI Color")]
    public class UIColor : ScriptableObject
    {
        [SerializeField] private Color color = Color.white;

        public UnityEngine.Color Color => color;
    }
}