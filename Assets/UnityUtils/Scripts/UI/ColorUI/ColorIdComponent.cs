﻿using System;
using UnityUtils.Extensions;
using UnityEngine.UI;
using UnityEngine;

namespace UnityUtils.UI
{
    [ExecuteAlways]
    public class ColorIdComponent : MonoBehaviour
    {
        [SerializeField] private ColorId colorId = null;
        [SerializeField] private ColorScheme scheme = null;
        [SerializeField] private bool updateChildren = false;

        public ColorScheme Scheme
        {
            get => scheme;
            set
            {
                if (scheme != value) UpdateColor(value);
                scheme = value;
            }
        }

        private void Start()
        {
            UpdateColor();
        }

        private void OnEnable()
        {
            UpdateColor();
        }

        private void OnValidate()
        {
            UpdateColor();
        }

        public void UpdateColor()
        {
            UpdateColor(scheme);
        }

        public ColorId ColorId
        {
            get => colorId;
            set
            {
                ColorId previous = colorId;
                colorId = value;
                if (previous != colorId && scheme != null) UpdateColor(scheme);
            }
        }

        public void UpdateColor(ColorScheme newScheme)
        {
            scheme = newScheme;
            if (scheme != null)
            {
                Color color = colorId != null ? scheme.ColorFor(colorId) : Color.white;
                var graphic = gameObject.GetInterface<Graphic>();
                if (graphic != null)
                    graphic.color = color;
            }

            if (updateChildren)
            {
                foreach (var colorIdComponent in gameObject.GetComponentsInChildren<ColorIdComponent>())
                {
                    if (colorIdComponent == this) continue;
                    colorIdComponent.UpdateColor(newScheme);
                }
            }
            
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
#endif
        }
    }
}