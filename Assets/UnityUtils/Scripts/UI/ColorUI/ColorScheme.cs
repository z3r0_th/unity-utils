﻿using System;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Random = System.Random;

namespace UnityUtils.UI
{
    [CreateAssetMenu(fileName = "scheme", menuName = "UnityUtils/Color/Scheme")]
    public class ColorScheme : ScriptableObject
    {
        [SerializeField] private ColorDesign[] colors = null;

        public Color ColorFor(ColorId id)
        {
            return colors.First(design => design.colorId == id).color.Color;
        }

        [System.Serializable]
        private struct ColorDesign
        {
            public ColorId colorId;
            public UIColor color;

            public ColorDesign(UIColor color, ColorId colorId)
            {
                this.colorId = colorId;
                this.color = color;
            }
        }
    }
}