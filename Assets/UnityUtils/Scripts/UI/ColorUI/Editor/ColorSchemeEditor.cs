﻿using UnityUtils.UI;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ColorScheme))]
public class ColorSchemeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Update"))
        {
            var components = FindObjectsOfType<ColorIdComponent>();
            var progress = 0.0f;
            var length = (float) components.Length;
            Debug.Log($"<i>Updating [{components.Length}] components in scene</i>");
            for (int i = 0 ; i < components.Length ; ++i)
            {
                ColorIdComponent colorSchemeComponent = components[i];
                colorSchemeComponent.UpdateColor();                
                progress = i/length;
                EditorUtility.DisplayProgressBar("Update color", "Updating components tint colors in scene", progress);
            }
            
            EditorUtility.ClearProgressBar();
        }
    }
}
