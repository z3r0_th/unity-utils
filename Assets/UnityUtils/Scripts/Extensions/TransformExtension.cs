﻿using System.Text.RegularExpressions;
using System.Collections.Generic;
using UnityEngine;

namespace UnityUtils.Extensions {

    public static class TransformExtension
    {

#region GameObject extension access

        public static T GetOrAddComponent<T>(this Transform transform) where T : Component
        {
            return transform.gameObject.GetOrAddComponent<T>();
        }

        public static bool HasComponent<T>(this Transform transform) where T : Component
        {
            return transform.gameObject.HasComponent<T>();
        }

        public static T[] GetComponentsInChildrenRecursively<T>(this Transform transform,
            bool includeInactive = false) where T : Component
        {
            return transform.gameObject.GetComponentsInChildrenRecursively<T>(includeInactive);
        }

        public static Transform GetChildNamed(this Transform transform, string childName, bool deepSearch = false)
        {
            GameObject go = transform.gameObject.GetChildNamed(childName, deepSearch);
            if (go == null) return null;

            return go.transform;
        }

        public static Transform GetChildNamed(this Transform transform, Regex regex, bool deepSearch = false)
        {
            GameObject go = transform.gameObject.GetChildNamed(regex, deepSearch);
            if (go == null) return null;

            return go.transform;
        }

        public static Transform[] GetChildrenNamed(this Transform transform, Regex regex, bool deepSearch = false)
        {
            List<Transform> list = new List<Transform>();
            transform.GetChildrenNamed(regex, deepSearch, list);
            return list.ToArray();
        }

        public static void GetChildrenNamed(this Transform transform, Regex regex, bool deepSearch, List<Transform> list)
        {
            foreach (Transform transformChild in transform)
            {
                if (regex.IsMatch(transformChild.name)) list.Add(transformChild);

                if (deepSearch)
                {
                    transformChild.GetChildrenNamed(regex, true, list);
                }
            }
        }

        public static bool IsAssetPrefab(this Transform transform)
        {
            return transform.gameObject.IsAssetPrefab();
        }

        #endregion

    }

}
