﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using UnityEditor;
using UnityEngine;

namespace UnityUtils.EditorHelper
{
    public static class EditorExtensions
    {
        public static Type GetPropertyType(this SerializedProperty property)
        {
            if (property.serializedObject == null || property.serializedObject.targetObject == null) return null;
            
            FieldInfo fieldInfo = null;
            var parent = property.serializedObject.targetObject.GetType();
            var fieldPathPart = property.propertyPath.Split ('.');

            BindingFlags flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic |
                                 BindingFlags.GetField | BindingFlags.SetField;
            
            for (int i = 0; i < fieldPathPart.Length; i++)
            {
                if (parent == null) return null;
                
                fieldInfo = parent.GetField (fieldPathPart[i], flags);
                if (fieldInfo == null)
                {
                    throw new Exception($"Could not retrieve serialized property field info path[{fieldPathPart[i]}] + parent[{parent.Name}] + fullPath[{property.propertyPath}]");
                }
     
                // there are only two container field type that can be serialized:
                // Array and List<T>
                if (fieldInfo.FieldType.IsArray) 
                {
                    parent = fieldInfo.FieldType.GetElementType ();
                    i += 2;
                    continue;
                }
 
                if (fieldInfo.FieldType.IsGenericType) 
                {
                    parent = fieldInfo.FieldType.GetGenericArguments () [0];
                    i += 2;
                    continue;
                }

                parent = fieldInfo.FieldType;
            }

            return fieldInfo == null ? null : fieldInfo.FieldType;
        }
        
        public static T[] GetPropertyAttributes<T>(this SerializedProperty property) where T : System.Attribute
        {
            System.Reflection.BindingFlags bindingFlags = System.Reflection.BindingFlags.GetField
                                                          | System.Reflection.BindingFlags.GetProperty
                                                          | System.Reflection.BindingFlags.IgnoreCase
                                                          | System.Reflection.BindingFlags.Instance
                                                          | System.Reflection.BindingFlags.NonPublic
                                                          | System.Reflection.BindingFlags.Public;
            if (property.serializedObject.targetObject == null)
                return null;
            var targetType = property.serializedObject.targetObject.GetType();
            var field = targetType.GetField(property.name, bindingFlags);
            if (field != null)
                return Util.ConvertToArray<T>(field.GetCustomAttributes(typeof(T), true));
            return null;
        }
        
        public static T GetPropertyAttribute<T>(this SerializedProperty property) where T : System.Attribute
        {
            System.Reflection.BindingFlags bindingFlags = System.Reflection.BindingFlags.GetField
                                                          | System.Reflection.BindingFlags.GetProperty
                                                          | System.Reflection.BindingFlags.IgnoreCase
                                                          | System.Reflection.BindingFlags.Instance
                                                          | System.Reflection.BindingFlags.NonPublic
                                                          | System.Reflection.BindingFlags.Public;
            if (property.serializedObject.targetObject == null)
                return null;
            var targetType = property.serializedObject.targetObject.GetType();
            var field = targetType.GetField(property.name, bindingFlags);
            if (field != null)
                return field.GetCustomAttribute(typeof(T), true) as T;
            return null;
        }
    }
}
