﻿using UnityEngine;

namespace UnityUtils.Variables.Boolean
{
    [CreateAssetMenu(fileName = "Bool", menuName = "Variables/Boolean")]
    public class UBoolObject : VariableObject<bool>
    { }
}
