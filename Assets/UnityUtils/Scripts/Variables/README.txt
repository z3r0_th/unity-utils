# How to create a UVariable:

* Create the variable object, extending the "VariableObject<>" class and adding the menu attribute. For exemple, the bool object would be:

1. [CreateAssetMenu(fileName = "Bool", menuName = "Variables/Boolean")] public class UBoolObject : VariableObject<bool> { }

* Create a struct of your "UVariable" type, for example UBool. This struct MUST extend IVariable<bool> (use the type you wish to support) and have the fields:

1. objectValue, of type of the specific object type for this variable (UBoolObject, for example)
2. bool viewObjectValue
3. bool singleValue
4. bool useSingle

* you can add implicit and explicit operators to ease the use of these variables

* Now in the Editor folder, create at least two classes:

1. UBooleanObjectEditor, with CustomEditor attribute and extending the VariableObjectEditor:

[CustomEditor(typeof(UBoolObject), true)] public class UBooleanObjectEditor : VariableObjectEditor { }

2. UBooleanPropertyDrawer, with CustomPropertyDrawer attribute and extending the VariableDrawer:

[CustomPropertyDrawer(typeof(UBool), true)] public class UBooleanPropertyDrawer : VariableDrawer { }

* Thats it, this is the basic variable criation. You may support attributes and variable different types (as in FloatObject), than just follow the float workflow.