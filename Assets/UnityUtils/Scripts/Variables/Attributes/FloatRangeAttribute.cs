﻿using UnityEngine;

namespace UnityUtils.Variables
{
    public class FloatRangeAttribute : PropertyAttribute
    {
        public float Max;
        public float Min;

        public FloatRangeAttribute(float min, float max)
        {
            this.Max = max;
            this.Min = min;
        }
    }
}
