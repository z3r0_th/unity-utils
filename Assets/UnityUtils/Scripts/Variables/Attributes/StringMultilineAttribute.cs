﻿using UnityEngine;

namespace UnityUtils.Variables
{
    public class StringMultilineAttribute : PropertyAttribute
    {
        public float Lines;

        public StringMultilineAttribute()
        {
            Lines = 3;
        }
        
        public StringMultilineAttribute(float lines)
        {
            Lines = Mathf.Max(lines, 3);
        }

    }
}