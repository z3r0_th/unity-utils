﻿using UnityEngine;

namespace UnityUtils.Variables
{
    public class VariableAttribute : PropertyAttribute
    {
        public readonly System.Type VariableType;

        public VariableAttribute(System.Type variableType)
        {
            if (!typeof(IVariableObject).IsAssignableFrom(variableType))
            {
                Debug.LogError($"<color=red>TYPE MUST BE <b>\"VariableObject<>\"</b></color>. It is: {variableType.Name}");
            }
        
            VariableType = variableType;
        }
    }
}
