﻿using UnityEngine;

namespace UnityUtils.Variables
{
    public class StringTextAreaAttribute : PropertyAttribute
    {
        public float Height;

        public StringTextAreaAttribute()
        {
            Height = 60;
        }
    
        public StringTextAreaAttribute(float height)
        {
            Height = Mathf.Max(height, 60);
        }
    }
}
