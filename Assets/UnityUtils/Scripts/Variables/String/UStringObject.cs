﻿using UnityEngine;

namespace UnityUtils.Variables.String
{
    [CreateAssetMenu(fileName = "String", menuName = "Variables/String")]
    public class UStringObject : VariableObject<string>
    { }
}
