﻿using UnityUtils.Variables.Floats;
using UnityEngine;

namespace UnityUtils.Variables
{
    [System.Serializable]
    public struct UFloat : IVariable<float>
    {
        #region Constructor
        
        public UFloat(float f)
        {
            useSingle = true;
            singleValue = f;
            objectValue = null;
            viewObjectValue = false;
            isInfinity = false;
            singleAuxFloatValue = 0;
            singleIsRandom = false;
        }

        public UFloat(FloatObject obj)
        {
            useSingle = false;
            objectValue = obj;
            singleValue = 0;
            viewObjectValue = false;
            isInfinity = false;
            singleAuxFloatValue = 0;
            singleIsRandom = false;
        }

        public UFloat(UFloat f)
        {
            useSingle = f.useSingle;
            objectValue = f.objectValue;
            singleValue = f.singleValue;
            
            viewObjectValue = f.viewObjectValue;
            isInfinity = f.isInfinity;
            singleAuxFloatValue = f.singleAuxFloatValue;
            singleIsRandom = f.singleIsRandom;
        }
        
        #endregion
        
        public float Value
        {
            get
            {
                if (useSingle)
                {
                    if (singleIsRandom) return Random.Range(singleAuxFloatValue, singleValue);
                    return singleValue;
                }
                
                if (objectValue == null) return 0;
                return objectValue.Value;
            }

            set
            {
                if (useSingle) singleValue = value;
                if (objectValue != null) objectValue.Value = value;
            }
        }

        public bool IsInfinity => isInfinity;
        
        [SerializeField] private float singleAuxFloatValue;
        [SerializeField] private FloatObject objectValue;
        [SerializeField] private bool viewObjectValue;
        [SerializeField] private bool singleIsRandom;
        [SerializeField] private float singleValue;
        [SerializeField] private bool isInfinity;
        [SerializeField] private bool useSingle;

        public static implicit operator float(UFloat f) => f.Value;
        public static implicit operator UFloat(float f) => new UFloat(f);
        public static implicit operator UFloat(FloatObject f) => new UFloat(f);
    }
    
    //TODO: Refactor the random aux variables (singleAuxFloatValue and singleIsRandom)
}