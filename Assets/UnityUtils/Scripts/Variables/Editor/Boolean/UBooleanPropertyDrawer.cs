﻿using UnityUtils.Scripts.Variables.Editor.Common;
using UnityUtils.Variables;
using UnityEditor;

namespace UnityUtils.Scripts.Variables.Editor.Boolean
{
    [CustomPropertyDrawer(typeof(UBool), true)]
    public class UBooleanPropertyDrawer : VariableDrawer
    { }
}
