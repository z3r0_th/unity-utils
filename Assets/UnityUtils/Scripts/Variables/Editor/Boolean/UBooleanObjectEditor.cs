﻿using UnityUtils.Variables.Boolean;
using UnityEditor;

[CustomEditor(typeof(UBoolObject), true)]
public class UBooleanObjectEditor : VariableObjectEditor
{ }
