﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityUtils.Variables;

public class VariableObjectDrawer
{
    protected SerializedObject serializedObject;
    private Object target;
    
    public SerializedProperty OriginalFieldSerializedProperty;
    public bool ShowSingleValue;
    
    
    public virtual void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        UpdateObject(position, property, label);
        if (serializedObject == null || target == null) return;
        ShowValueOnPlayCondition(position, property, label);
        serializedObject.ApplyModifiedProperties();
    }

    public virtual float SingleHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label);
    }
    
    public virtual float VariableHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label);
    }
    
    public virtual void ShowSingle(Rect position, SerializedProperty property, GUIContent label)
    {
        if (OriginalFieldSerializedProperty.FindPropertyRelative("singleIsRandom") != null)
            OriginalFieldSerializedProperty.FindPropertyRelative("singleIsRandom").boolValue = false;
        EditorGUI.PropertyField(position, property, label);
    }
    
    protected virtual void ShowValue(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.PropertyField(position, property, label);
    }

    protected virtual void ShowValueRealtime(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginDisabledGroup(true);
        EditorGUI.PropertyField(position, property, label);

        //TODO: Add Save button
        EditorGUI.EndDisabledGroup();
    }

    #region Private Methods

    private void UpdateObject(Rect position, SerializedProperty property, GUIContent label)
    {
        target = property.objectReferenceValue;
        if (target == null)
        {
            EditorGUI.PropertyField(position, property, label);
            return;
        }
        
        if (serializedObject == null) serializedObject = new SerializedObject(target);
        serializedObject.UpdateIfRequiredOrScript();
    }
    
    private void ShowValueOnPlayCondition(Rect position, SerializedProperty property, GUIContent label)
    {
        if (ShowSingleValue)
        {
            if (OriginalFieldSerializedProperty?.FindPropertyRelative("singleValue") != null)
                ShowSingle(position, OriginalFieldSerializedProperty.FindPropertyRelative("singleValue"), label);
            else Debug.LogError("Unexpected error with single value property");
            
            return;
        }
        
        if (!Application.isPlaying)
        {
            ShowValue(position, serializedObject.FindProperty("value"), label);
        }
        else
        {
            ShowValueRealtime(position, serializedObject.FindProperty("realtimeValue"), label);
        }

        serializedObject.ApplyModifiedProperties();
    }

    #endregion
    
}
