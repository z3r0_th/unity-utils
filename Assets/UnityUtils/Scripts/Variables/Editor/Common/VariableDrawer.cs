﻿using UnityUtils.EditorHelper;
using UnityUtils.Variables;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using System;
using UnityEngine.UIElements;

namespace UnityUtils.Scripts.Variables.Editor.Common
{
    /// <summary>
    /// By Convention, the property MUST have the following serialized fields:
    /// Object objectValue (a Scriptable object that extends "VariableObject<T>. Replace the Object by a direct reference to the class")
    /// bool useSingle
    /// bool viewObjectValue
    /// </summary>
    public class VariableDrawer : PropertyDrawer
    {
        private VariableObjectDrawer customPropertyDrawer;
        private VariableAttribute variableAttribute;
        private UnityEngine.Object lastReference;
        private bool anyObject;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            DrawVariableContextOptionMenu(position, property, label);

            if (CustomGUI(position, property, label))
            {
                EditorGUI.EndProperty();
                return;
            }
            
            if (lastReference == null || lastReference != property.FindPropertyRelative("objectValue").objectReferenceValue)
            {
                lastReference = property.FindPropertyRelative("objectValue").objectReferenceValue;
                GetVariableAttributeAndCustomDrawer(property);
            }
            
            if (property.FindPropertyRelative("useSingle").boolValue)
            {
                DrawSingle(position, property, label);
                EditorGUI.EndProperty();
                return;
            }

            if (property.FindPropertyRelative("objectValue").objectReferenceValue != null && 
                property.FindPropertyRelative("viewObjectValue").boolValue)
            {
                DrawObjectValue(position, property, label);
            }
            else
            {
                DrawObjectField(position, property, label);
            }
            
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (property.FindPropertyRelative("useSingle").boolValue)
            {
                return customPropertyDrawer?.SingleHeight(property.FindPropertyRelative("singleValue"), label) 
                       ?? EditorGUI.GetPropertyHeight(property.FindPropertyRelative("singleValue"), label);
            }

            return customPropertyDrawer?.VariableHeight(property, label) 
                   ?? EditorGUI.GetPropertyHeight(property.FindPropertyRelative("objectValue"), label);
        }

        private void DrawObjectValue(Rect position, SerializedProperty property, GUIContent label)
        {
            var originalObject = property.FindPropertyRelative("objectValue").objectReferenceValue;
            var serializedObject = new SerializedObject(originalObject);
            serializedObject.UpdateIfRequiredOrScript();
                
            label.tooltip = serializedObject.targetObject.name;
                
            if (customPropertyDrawer != null)
            {
                customPropertyDrawer.ShowSingleValue = false;
                customPropertyDrawer.OriginalFieldSerializedProperty = property;
                customPropertyDrawer.OnGUI(position, property.FindPropertyRelative("objectValue"), label);
            }
            else
            {
                if (serializedObject.FindProperty("value") != null &&
                    serializedObject.FindProperty("realtimeValue") != null)
                {
                    EditorGUI.PropertyField(position, Application.isPlaying ? serializedObject.FindProperty("realtimeValue") : serializedObject.FindProperty("value"), label);
                    serializedObject.ApplyModifiedProperties();
                }
                else
                {
                    EditorGUI.PropertyField(position, property.FindPropertyRelative("objectValue"), label);                    
                }
                
            }
        }

        private void DrawObjectField(Rect position, SerializedProperty property, GUIContent label)
        {
            if (variableAttribute != null && variableAttribute.VariableType != null && !anyObject)
                EditorGUI.ObjectField(position, property.FindPropertyRelative("objectValue"), variableAttribute.VariableType, label);
            else
                EditorGUI.ObjectField(position, property.FindPropertyRelative("objectValue"), property.FindPropertyRelative("objectValue").GetPropertyType(), label);
        }


        /// <summary>
        /// Override this to draw your own custom property. This will keep the context menu and save features, but use your drawing method for the field
        /// </summary>
        /// <param name="position"></param>
        /// <param name="property"></param>
        /// <param name="label"></param>
        /// <returns>return false to use the default drawer, true to cancel the default drawer part</returns>
        protected virtual bool CustomGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            return false;
        }
        
        /// <summary>
        /// Using single/native variable instead of object one
        /// </summary>
        /// <param name="position"></param>
        /// <param name="property"></param>
        /// <param name="label"></param>
        protected virtual void DrawSingle(Rect position, SerializedProperty property, GUIContent label)
        {
            if (customPropertyDrawer != null)
            {
                customPropertyDrawer.ShowSingleValue = true;
                customPropertyDrawer.OriginalFieldSerializedProperty = property;
                customPropertyDrawer.ShowSingle(position, property.FindPropertyRelative("singleValue"), label);
            }
            else
            {
                EditorGUI.PropertyField(position, property.FindPropertyRelative("singleValue"), label);
            }
        }

        #region Context Menu

        /// <summary>
        /// Extend this method to add more context menu options
        /// </summary>
        /// <param name="menu"></param>
        /// <param name="property"></param>
        protected virtual void GenericContextMenu(GenericMenu menu, SerializedProperty property)
        { }

        /// <summary>
        /// This method generates the context menu (to select if single or using variable object and if view the value or object field)
        /// </summary>
        /// <param name="position"></param>
        /// <param name="property"></param>
        /// <param name="label"></param>
        protected virtual void ShowContextMenu(Rect position, SerializedProperty property, GUIContent label)
        {
            var useSingle = property.FindPropertyRelative("useSingle").boolValue;

            var menu = new GenericMenu();
            menu.AddItem(new GUIContent("Use Object"), !useSingle, () =>
            {
                property.FindPropertyRelative("useSingle").boolValue = !useSingle;
                property.serializedObject.ApplyModifiedProperties();
            });


            if (!useSingle)
            {
                var viewValue = property.FindPropertyRelative("viewObjectValue").boolValue;
                menu.AddItem(new GUIContent("View Value"), viewValue, () =>
                {
                    property.FindPropertyRelative("viewObjectValue").boolValue = !viewValue;
                    property.serializedObject.ApplyModifiedProperties();
                });
            }
            
            menu.AddSeparator("");
            GenericContextMenu(menu, property);
            menu.ShowAsContext();
        }

        private void DrawVariableContextOptionMenu(Rect position, SerializedProperty property, GUIContent label)
        {
            var icon = EditorGUIUtility.IconContent("_Menu");
            var centerY = (position.height - icon.image.height) / 2.0f;
            var iconRect = new Rect(position.x - icon.image.width, position.y + centerY, icon.image.width, icon.image.height);
            GUI.Label(iconRect, icon);

            var @event = Event.current;
            if (@event.type != EventType.ContextClick || @event.type == EventType.MouseUp ||
                !iconRect.Contains(@event.mousePosition)) return;

            ShowContextMenu(position, property, label);
        } 

        #endregion
        
        #region Private Methods
        
        private void GetVariableAttributeAndCustomDrawer(SerializedProperty property)
        {
            variableAttribute = property.GetPropertyAttribute<VariableAttribute>();
            anyObject = variableAttribute == null;
            if (variableAttribute == null)
            {
                Type targetType = null;
                targetType = property.FindPropertyRelative("objectValue").objectReferenceValue == null ? 
                    property.FindPropertyRelative("objectValue").GetPropertyType() : 
                    property.FindPropertyRelative("objectValue").objectReferenceValue.GetType();
                
                if (targetType == null) throw new Exception("Target Type cannot be null");
                
                variableAttribute = new VariableAttribute(targetType);
            }

            var drawer = GetDrawerClassWithAttributeToType(variableAttribute.VariableType);
            if (drawer != null && (customPropertyDrawer == null || drawer != customPropertyDrawer.GetType()))
            {
                customPropertyDrawer = (VariableObjectDrawer)Activator.CreateInstance(drawer);
            }
        }
        
        private Type GetDrawerClassWithAttributeToType(Type floatType)
        {
            Assembly assembly = Assembly.GetAssembly(GetType());
            var attributeType = typeof(CustomPropertyDrawer);
            var attributeFieldType = attributeType.GetField("m_Type",
                BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
            if (attributeFieldType == null)
            {
                Debug.LogError("Should have the m_type field");
                return null;
            }

            foreach (Type type in assembly.GetTypes())
            {
                var drawerAttribute = type.GetCustomAttribute(typeof(CustomPropertyDrawer), true);
                if (drawerAttribute == null) continue;
                var drawType = (Type) attributeFieldType.GetValue(drawerAttribute);
                if (drawType == null || drawType != floatType) continue;

                return type;
            }

            return null;
        }
        
        #endregion
    }
}
