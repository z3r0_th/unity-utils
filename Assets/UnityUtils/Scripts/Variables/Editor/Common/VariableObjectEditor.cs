﻿using UnityEngine;
using UnityEditor;

public class VariableObjectEditor : Editor
{
    public SerializedProperty ValueProperty { get; private set; }

    public override void OnInspectorGUI()
    {
        serializedObject.UpdateIfRequiredOrScript();
        string valuePropertyName = Application.isPlaying ? "realtimeValue" : "value";
        string valuePropertyToHideName = Application.isPlaying ? "value" : "realtimeValue";
        ValueProperty = serializedObject.FindProperty(valuePropertyName);
        DrawPropertiesExcluding(serializedObject, valuePropertyName, valuePropertyToHideName);
        ShowValue();
        
        if (Application.isPlaying)
        {
            if (GUILayout.Button("Save Value"))
            {
                Save();
            }
        }
        
        serializedObject.ApplyModifiedProperties();
    }

    protected virtual void Save()
    {
        SerializedProperty saveIntoProperty = serializedObject.FindProperty("value");
        SerializedProperty saveFromProperty = serializedObject.FindProperty("realtimeValue");

        if (saveIntoProperty.propertyType == SerializedPropertyType.Float) 
        {
            saveIntoProperty.floatValue = saveFromProperty.floatValue;
        } else if (saveIntoProperty.propertyType == SerializedPropertyType.Boolean)
        {
            saveIntoProperty.boolValue = saveFromProperty.boolValue;
        }
        else
        {
            Debug.LogError($"Type {saveIntoProperty.propertyType.ToString()} not expected");
        }
        
    }

    protected virtual void ShowValue()
    {
        EditorGUILayout.PropertyField(ValueProperty);
    }
}
