﻿using UnityUtils.Scripts.Variables.Editor.Common;
using UnityUtils.Variables;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(UFloat), true)]
public class UFloatPropertyDrawer : VariableDrawer
{
    protected override void GenericContextMenu(GenericMenu menu, SerializedProperty property)
    {
        var infinity = property.FindPropertyRelative("isInfinity").boolValue;
        menu.AddItem(new GUIContent("Infinity"), infinity, () =>
        {
            property.FindPropertyRelative("isInfinity").boolValue = !infinity;
            property.serializedObject.ApplyModifiedProperties();
        });
    }
    
    protected override bool CustomGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (property.FindPropertyRelative("isInfinity").boolValue)
        {
            EditorGUI.BeginDisabledGroup(true);
            EditorGUI.LabelField(position, label, new GUIContent("infinity"));
            EditorGUI.EndDisabledGroup();
            EditorGUI.EndProperty();
            return true;
        }
        
        return false;
    }
}
