﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEditor;
using UnityEngine;
using UnityUtils.Variables.Floats;

[CustomEditor(typeof(RandomFloat), true)]
public class RandomFloatEditor : Editor
{
    public override void OnInspectorGUI()
    {
        serializedObject.UpdateIfRequiredOrScript();
        string minValueString = Application.isPlaying ? "minValueRealtime" : "minValue";
        string maxValueString = Application.isPlaying ? "maxValueRealtime" : "maxValue";
        
        string minString = Application.isPlaying ? "minRealtime" : "min";
        string maxString = Application.isPlaying ? "maxRealtime" : "max";
        
        DrawPropertiesExcluding(serializedObject, "value", "realtimeValue", "minValue", "maxValue", "min", "max",
            "minValueRealtime","maxValueRealtime","minRealtime","maxRealtime");

        float minValue = serializedObject.FindProperty(minValueString).floatValue;
        float maxValue = serializedObject.FindProperty(maxValueString).floatValue;
        
        float min = serializedObject.FindProperty(minString).floatValue;
        float max = serializedObject.FindProperty(maxString).floatValue;

        EditorGUILayout.PropertyField(serializedObject.FindProperty(minString));
        EditorGUILayout.PropertyField(serializedObject.FindProperty(maxString));
        EditorGUILayout.MinMaxSlider(ref minValue, ref maxValue, min, max);

        serializedObject.FindProperty(minValueString).floatValue = minValue;
        serializedObject.FindProperty(maxValueString).floatValue = maxValue;
        
        EditorGUILayout.LabelField(new GUIContent($"[{minValue.ToString("F2", CultureInfo.InvariantCulture)},{maxValue.ToString("F2", CultureInfo.InvariantCulture)}]"));
        
        if (Application.isPlaying)
        {
            if (GUILayout.Button("Save Value"))
            {
                serializedObject.FindProperty("minValue").floatValue = serializedObject.FindProperty(minValueString).floatValue;
                serializedObject.FindProperty("maxValue").floatValue = serializedObject.FindProperty(maxValueString).floatValue;
        
                serializedObject.FindProperty("min").floatValue = serializedObject.FindProperty(minString).floatValue;
                serializedObject.FindProperty("max").floatValue = serializedObject.FindProperty(maxString).floatValue;
            }
        }

        serializedObject.ApplyModifiedProperties();
    }
}
