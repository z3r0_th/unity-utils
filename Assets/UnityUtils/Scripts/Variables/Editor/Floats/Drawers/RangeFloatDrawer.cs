﻿using System.Globalization;
using UnityUtils.Variables.Floats;
using UnityUtils.EditorHelper;
using UnityUtils.Variables;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(RangeFloat))]
public class RangeFloatDrawer : FloatObjectDrawer
{
    private FloatRangeAttribute rangeAttribute;
    
    protected override void ShowValue(Rect position, SerializedProperty property, GUIContent label)
    {
        if (serializedObject.FindProperty("min") == null ||
            serializedObject.FindProperty("max") == null)
        {
            Debug.LogError("Object should have max and min properties");
        }
        
        if (rangeAttribute == null)
        {
            rangeAttribute = OriginalFieldSerializedProperty.GetPropertyAttribute<FloatRangeAttribute>();
            if (rangeAttribute != null && rangeAttribute.Min >= rangeAttribute.Max)
            {
                Debug.LogError("Attribute min is bigger than max");
            }
        }
        
        var min = serializedObject.FindProperty("min").floatValue;
        var max = serializedObject.FindProperty("max").floatValue;

        if (rangeAttribute != null)
        {
            if (Mathf.Abs(rangeAttribute.Max - max) > float.Epsilon || Mathf.Abs(rangeAttribute.Min - min) > float.Epsilon)
            {
                var icon = EditorGUIUtility.IconContent("console.warnicon.sml");
                if (rangeAttribute.Max > max || rangeAttribute.Min < min) icon = EditorGUIUtility.IconContent("console.erroricon.sml");
                var centerY = (position.height - icon.image.height) / 2.0f;
                var iconRect = new Rect(position.x, position.y + centerY, icon.image.width, icon.image.height);
                icon.tooltip = $"Range restriction ({rangeAttribute.Min.ToString("F1", CultureInfo.InvariantCulture)}, {rangeAttribute.Max.ToString("F1", CultureInfo.InvariantCulture)}) - object:({min.ToString("F1", CultureInfo.InvariantCulture)},{max.ToString("F1", CultureInfo.InvariantCulture)})";
                GUI.Label(iconRect, icon);
                position.x += iconRect.width;
                position.width -= iconRect.width;
            }
            
            max = rangeAttribute.Max;
            min = rangeAttribute.Min;
        }

        EditorGUI.Slider(position, property, min, max, label);
    }

    public override void ShowSingle(Rect position, SerializedProperty property, GUIContent label)
    {
        if (rangeAttribute == null)
        {
            rangeAttribute = OriginalFieldSerializedProperty.GetPropertyAttribute<FloatRangeAttribute>();
            if (rangeAttribute != null && rangeAttribute.Min >= rangeAttribute.Max)
            {
                Debug.LogError("Attribute min is bigger than max");
            }
        }

        var min = 0.0f;
        var max = 1.0f;
        if (rangeAttribute != null)
        {
            min = rangeAttribute.Min;
            max = rangeAttribute.Max;
        }

        EditorGUI.Slider(position, property, min, max, label);
    }
}
