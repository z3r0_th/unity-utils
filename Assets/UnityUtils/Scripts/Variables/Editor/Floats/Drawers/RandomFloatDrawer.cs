﻿using UnityUtils.Variables.Floats;
using UnityUtils.EditorHelper;
using System.Globalization;
using UnityUtils.Variables;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(RandomFloat))]
public class RandomFloatDrawer : FloatObjectDrawer
{
    private FloatRangeAttribute rangeAttribute;
    
    protected override void ShowValue(Rect position, SerializedProperty property, GUIContent label)
    {
        if (serializedObject.FindProperty("min") == null ||
            serializedObject.FindProperty("max") == null)
        {
            Debug.LogError("Object should have max and min properties");
        }
        
        if (rangeAttribute == null)
        {
            rangeAttribute = OriginalFieldSerializedProperty.GetPropertyAttribute<FloatRangeAttribute>();
            if (rangeAttribute != null && rangeAttribute.Min >= rangeAttribute.Max)
            {
                Debug.LogError("Attribute min is bigger than max");
            }
        }

        var minString = Application.isPlaying ? "minRealtime" : "min";
        var maxString = Application.isPlaying ? "maxRealtime" : "max";
        var minValueString = Application.isPlaying ? "minValueRealtime" : "minValue";
        var maxValueString = Application.isPlaying ? "maxValueRealtime" : "maxValue";
        
        var min = serializedObject.FindProperty(minString).floatValue;
        var max = serializedObject.FindProperty(maxString).floatValue;
        
        var minValue = serializedObject.FindProperty(minValueString).floatValue;
        var maxValue = serializedObject.FindProperty(maxValueString).floatValue;
        
        label.tooltip = label.tooltip + $" [{minValue.ToString("F1", CultureInfo.InvariantCulture)},{maxValue.ToString("F1", CultureInfo.InvariantCulture)}]";
        label.text = label.text + $" [{minValue.ToString("F1", CultureInfo.InvariantCulture)},{maxValue.ToString("F1", CultureInfo.InvariantCulture)}]"; 
        
        if (rangeAttribute != null)
        {
            if (Mathf.Abs(rangeAttribute.Max - max) > float.Epsilon || Mathf.Abs(rangeAttribute.Min - min) > float.Epsilon)
            {
                var icon = EditorGUIUtility.IconContent("console.warnicon.sml");
                if (rangeAttribute.Max > max || rangeAttribute.Min < min) icon = EditorGUIUtility.IconContent("console.erroricon.sml");
                var centerY = (position.height - icon.image.height) / 2.0f;
                var iconRect = new Rect(position.x, position.y + centerY, icon.image.width, icon.image.height);
                icon.tooltip = $"Range restriction ({rangeAttribute.Min}, {rangeAttribute.Max}) - object:({min},{max})";
                GUI.Label(iconRect, icon);
                position.x += iconRect.width;
                position.width -= iconRect.width;
            }
            
            max = rangeAttribute.Max;
            min = rangeAttribute.Min;
        }

        EditorGUI.MinMaxSlider(position, label, ref minValue, ref maxValue, min, max);
        serializedObject.FindProperty(minValueString).floatValue = minValue;
        serializedObject.FindProperty(maxValueString).floatValue = maxValue;
    }

    public override void ShowSingle(Rect position, SerializedProperty property, GUIContent label)
    {
        if (rangeAttribute == null)
        {
            rangeAttribute = OriginalFieldSerializedProperty.GetPropertyAttribute<FloatRangeAttribute>();
            if (rangeAttribute != null && rangeAttribute.Min >= rangeAttribute.Max)
            {
                Debug.LogError("Attribute min is bigger than max");
            }
        }
        
        OriginalFieldSerializedProperty.FindPropertyRelative("singleIsRandom").boolValue = true;
        var min = 0.0f;
        var max = 1.0f;
        var minValue = OriginalFieldSerializedProperty.FindPropertyRelative("singleAuxFloatValue").floatValue;
        var maxValue = property.floatValue;
        if (rangeAttribute != null)
        {
            min = rangeAttribute.Min;
            max = rangeAttribute.Max;
        }
        
        label.text = label.text + $" [{minValue.ToString("F1", CultureInfo.InvariantCulture)},{maxValue.ToString("F1", CultureInfo.InvariantCulture)}]";
        EditorGUI.MinMaxSlider(position, label, ref minValue, ref maxValue, min, max);
        OriginalFieldSerializedProperty.FindPropertyRelative("singleAuxFloatValue").floatValue = minValue;
        property.floatValue = maxValue;
    }
}
