﻿using UnityUtils.Variables.Floats;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(NegativeFloat))]
public class NegativeFloatDrawer : FloatObjectDrawer
{
    protected override void ShowValue(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.PropertyField(position, property, label);
        property.floatValue = Mathf.Min(property.floatValue, 0);
    }

    public override void ShowSingle(Rect position, SerializedProperty property, GUIContent label)
    {
        base.ShowSingle(position, property, label);
        property.floatValue = Mathf.Min(property.floatValue, 0);
    }
}