﻿using UnityUtils.Variables.Floats;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(FloatObject))]
public class FloatObjectDrawer : VariableObjectDrawer
{ }
