﻿using UnityUtils.Variables.Floats;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PositiveFloat))]
public class PositveFloatEditor : FloatObjectEditor
{
    protected override void ShowValue()
    {
        EditorGUILayout.PropertyField(ValueProperty);
        ValueProperty.floatValue = Mathf.Max(0, ValueProperty.floatValue);
    }
}
