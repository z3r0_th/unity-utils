﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityUtils.Variables.Floats;

[CustomEditor(typeof(FloatObject), true)]
public class FloatObjectEditor : VariableObjectEditor
{ }
