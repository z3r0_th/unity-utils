﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityUtils.Variables.Floats;

[CustomEditor(typeof(RangeFloat))]
public class RangeFloatEditor : FloatObjectEditor
{
    protected override void ShowValue()
    {
        var max = serializedObject.FindProperty("max").floatValue;
        var min = serializedObject.FindProperty("min").floatValue;
        if (max <= min) serializedObject.FindProperty("max").floatValue = min + 1;
        EditorGUILayout.Slider(ValueProperty, min, max, "Value");
    }
}
