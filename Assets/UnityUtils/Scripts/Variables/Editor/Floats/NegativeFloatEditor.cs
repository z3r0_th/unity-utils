﻿using UnityUtils.Variables.Floats;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(NegativeFloat))]
public class NegativeFloatEditor : FloatObjectEditor
{
    protected override void ShowValue()
    {
        EditorGUILayout.PropertyField(ValueProperty);
        ValueProperty.floatValue = Mathf.Min(0, ValueProperty.floatValue);
    }
}
