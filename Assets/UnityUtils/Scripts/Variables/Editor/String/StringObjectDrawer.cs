﻿using UnityUtils.Variables.String;
using UnityUtils.EditorHelper;
using UnityUtils.Variables;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(UStringObject))]
public class StringObjectDrawer : VariableObjectDrawer
{
    private StringMultilineAttribute multilineAttribute;
    private StringTextAreaAttribute textAreaAttribute;
    private string lastStringValue = string.Empty;
    private Vector2 textScroll;

    protected override void ShowValue(Rect position, SerializedProperty property, GUIContent label)
    {
        ShowStringProperty(position, property, label);
    }

    public override float SingleHeight(SerializedProperty property, GUIContent label)
    {
        if (textAreaAttribute != null) return textAreaAttribute.Height + 15;
        if (multilineAttribute != null) return multilineAttribute.Lines*EditorStyles.textArea.lineHeight + 3;
            
        return base.SingleHeight(property, label);
    }

    public override float VariableHeight(SerializedProperty property, GUIContent label)
    {
        if (textAreaAttribute != null) return textAreaAttribute.Height + 15;
        if (multilineAttribute != null) return multilineAttribute.Lines*EditorStyles.textArea.lineHeight + 3;
        
        return base.VariableHeight(property, label);
    }
    
    public override void ShowSingle(Rect position, SerializedProperty property, GUIContent label)
    {
        ShowStringProperty(position, property, label);
    }

    private void GetStringAttributes()
    {
        if (textAreaAttribute == null)
        {
            textAreaAttribute = OriginalFieldSerializedProperty.GetPropertyAttribute<StringTextAreaAttribute>();
        } 
        if (multilineAttribute == null)
        {
            multilineAttribute = OriginalFieldSerializedProperty.GetPropertyAttribute<StringMultilineAttribute>();
        }
    }
    
    private void ShowStringProperty(Rect position, SerializedProperty property, GUIContent label,
        StringTextAreaAttribute stringTextAreaAttribute)
    {
        EditorGUI.LabelField(new Rect(position.x, position.y, position.width, 15), label);
        position.y += 15;
        position.height -= 15;
            
        GUIStyle style = EditorStyles.textArea;
        var viewRect = new Rect(position.x, position.y, 
            position.width - 15, 
            Mathf.Max(stringTextAreaAttribute.Height, style.CalcHeight(new GUIContent(property.stringValue), position.width))
        );
            
        textScroll = GUI.BeginScrollView(position, textScroll, new Rect(viewRect.x, viewRect.y, viewRect.width, viewRect.height), false, true);
        property.stringValue = EditorGUI.TextArea(viewRect, property.stringValue, style);
            
        if (lastStringValue != null && property.stringValue.Length > lastStringValue.Length &&
            property.stringValue.Length > 0 && lastStringValue.Length > 0 &&
            lastStringValue[lastStringValue.Length - 1] != property.stringValue[property.stringValue.Length - 1])
        {
            textScroll = new Vector2(textScroll.x, viewRect.height);
        }
            
        GUI.EndScrollView();
    }

    private void ShowStringProperty(Rect position, SerializedProperty property, GUIContent label)
    {
        GetStringAttributes();

        if (textAreaAttribute != null)
        {
            ShowStringProperty(position, property, label, textAreaAttribute);
        }
        else if (multilineAttribute != null)
        {
            position = EditorGUI.PrefixLabel(position, label);
            property.stringValue = EditorGUI.TextArea(position, property.stringValue);
        } else
        {
            base.ShowSingle(position, property, label);
        }

        lastStringValue = property.stringValue;
    }
}
