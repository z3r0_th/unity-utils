﻿using UnityUtils.Variables.String;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(UStringObject), true)]
public class UStringObjectEditor : VariableObjectEditor
{
    protected override void ShowValue()
    {
        EditorGUILayout.LabelField("Text");
        ValueProperty.stringValue = EditorGUILayout.TextArea(ValueProperty.stringValue);
    }
    
}
