﻿using UnityEditor;
using UnityUtils.Scripts.Variables.Editor.Common;
using UnityUtils.Variables;

namespace UnityUtils.Scripts.Variables.Editor.String
{
    [CustomPropertyDrawer(typeof(UString), true)]
    public class UStringPropertyDrawer : VariableDrawer
    { }
}