﻿using UnityEngine;

namespace UnityUtils.Variables
{
    public class VariableObject<T> : ScriptableObject, IVariableObject
    {
        public virtual T Value
        {
            get => Application.isPlaying ? realtimeValue : value;

            set
            {
                if (Application.isPlaying) realtimeValue = value;
                else this.value = value;
            }
        }

        [SerializeField] private T realtimeValue;
        [SerializeField] private T value;
        
        protected virtual void OnEnable()
        {
            realtimeValue = value;
        }
    }
}
