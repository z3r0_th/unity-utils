﻿using UnityUtils.Variables.String;
using UnityEngine;
using System;

namespace UnityUtils.Variables
{
    [Serializable]
    public struct UString : IVariable<string>
    {

        #region Constructor

        public UString(string s)
        {
            singleValue = s;
            useSingle = true;
            objectValue = null;

            viewObjectValue = false;
        }
        
        public UString(UStringObject s)
        {
            singleValue = s != null ? s.Value : "";
            useSingle = false;
            objectValue = s;

            viewObjectValue = false;
        }
        
        public UString(UString s)
        {
            singleValue = s.singleValue;
            useSingle = s.useSingle;
            objectValue = s.objectValue;

            viewObjectValue = s.viewObjectValue;
        }

        #endregion

        public string Value
        {
            get
            {
                if (useSingle) return singleValue;
                if (objectValue != null) return objectValue.Value;
                return string.Empty;
            }
            
            set
            {
                if (useSingle) singleValue = value;
                if (objectValue != null) objectValue.Value = value;
            }
        }
    
        [SerializeField] private UStringObject objectValue;
        [SerializeField] private bool viewObjectValue;
        [SerializeField] private string singleValue;
        [SerializeField] private bool useSingle;
    }
}
