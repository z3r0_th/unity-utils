﻿using UnityUtils.Variables.Boolean;
using UnityEngine;

namespace UnityUtils.Variables
{
    [System.Serializable]
    public struct UBool : IVariable<bool>
    {
        #region Constructor
        
        public UBool(bool b)
        {
            singleValue = b;
            useSingle = true;
            objectValue = null;

            viewObjectValue = false;
        }
        
        public UBool(UBoolObject b)
        {
            singleValue = b != null && b.Value;
            useSingle = false;
            objectValue = b;

            viewObjectValue = false;
        }
        
        public UBool(UBool b)
        {
            singleValue = b.singleValue;
            useSingle = b.useSingle;
            objectValue = b.objectValue;
            viewObjectValue = b.viewObjectValue;
        }
        
        #endregion
        
        public bool Value
        {
            get
            {
                if (useSingle) return singleValue;
                if (objectValue != null) return objectValue.Value;
                return false;
            }
            
            set
            {
                if (useSingle) singleValue = value;
                if (objectValue != null) objectValue.Value = value;
            }
        }

        [SerializeField] private UBoolObject objectValue;
        [SerializeField] private bool viewObjectValue;
        [SerializeField] private bool singleValue;
        [SerializeField] private bool useSingle;
        
        public static implicit operator bool(UBool b) => b.Value;
        public static implicit operator UBool(bool b) => new UBool(b);
        public static implicit operator UBool(UBoolObject b) => new UBool(b);
    }
}
