﻿using UnityEngine;

namespace UnityUtils.Variables.Floats
{
    [CreateAssetMenu(fileName = "Range Float", menuName = "Variables/Float/Random Float")]
    public class RandomFloat : FloatObject
    {
        [SerializeField] private float min = 0;
        [SerializeField] private float max = 1;
        
        [SerializeField] private float minValue = 0;
        [SerializeField] private float maxValue = 1;
        
        [SerializeField] private float minValueRealtime = 0;
        [SerializeField] private float maxValueRealtime = 0;
        [SerializeField] private float minRealtime = 0;
        [SerializeField] private float maxRealtime = 0;

        [SerializeField] private bool randomEveryTime = true;
        private bool randomized = false;

        public override float Value
        {
            get
            {
                if (!randomEveryTime && randomized) return base.Value;
                randomized = true;
                float minRange = Application.isPlaying ? minValueRealtime : minValue;
                float maxRange = Application.isPlaying ? maxValueRealtime : maxValue;
                base.Value = Random.Range(minRange, maxRange);
                
                return base.Value;
            }
            
            set => base.Value = Mathf.Clamp(value, Application.isPlaying ? 
                minValueRealtime : minValue, Application.isPlaying ? maxValueRealtime : maxValue);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            
            minValueRealtime = minValue;
            maxValueRealtime = maxValue;
            minRealtime = min;
            maxRealtime = max;
        }
    }
}
