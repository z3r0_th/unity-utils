﻿using UnityEngine;

namespace UnityUtils.Variables.Floats
{
    [CreateAssetMenu(fileName = "Range Float", menuName = "Variables/Float/Range Float")]
    public class RangeFloat : FloatObject
    {
        [SerializeField] private float min = 0;
        [SerializeField] private float max = 1;
        
        public override float Value
        {
            get => base.Value;
            set => base.Value = Mathf.Clamp(value, min, max);
        }
    }
}
