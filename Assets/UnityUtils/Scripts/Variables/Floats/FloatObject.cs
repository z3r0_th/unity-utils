﻿using UnityEngine;

namespace UnityUtils.Variables.Floats
{
    [CreateAssetMenu(fileName = "Float", menuName = "Variables/Float/Float")]
    public class FloatObject : VariableObject<float>
    { }
}
