﻿using UnityEngine;

namespace UnityUtils.Variables.Floats
{
    [CreateAssetMenu(fileName = "Positive Float", menuName = "Variables/Float/Positive Float")]
    public class PositiveFloat : FloatObject
    {
        public override float Value
        {
            get => base.Value;
            set => base.Value = Mathf.Max(0, value);
        }
    }
}
