﻿using UnityEngine;

namespace UnityUtils.Variables.Floats
{
    [CreateAssetMenu(fileName = "Negative Float", menuName = "Variables/Float/Negative Float")]
    public class NegativeFloat : FloatObject
    {
        public override float Value
        {
            get => base.Value;
            set => base.Value = Mathf.Min(0, value);
        }
    }
}
