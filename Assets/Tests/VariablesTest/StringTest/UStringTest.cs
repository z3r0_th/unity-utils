﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityUtils.Variables;

public class UStringTest : MonoBehaviour
{
    [SerializeField] private UString uString;
    
    [StringTextArea]
    [SerializeField] private UString uStringTextAreaObj;
    
    [StringTextArea]
    [SerializeField] private UString uStringTextAreaSingle;
    
    [StringMultiline]
    [SerializeField] private UString uStringMultilineSingle;

    //[Multiline]
    //public string stringMultiline;

    //[TextArea] public string stringTextArea;
}
