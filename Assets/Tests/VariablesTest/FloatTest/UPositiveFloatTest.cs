﻿using UnityEngine;
using UnityUtils.Variables;
using UnityUtils.Variables.Floats;

public class UPositiveFloatTest : MonoBehaviour
{
    [Variable(typeof(PositiveFloat))]
    public UFloat uPositiveFloat;

    private void Start()
    {
        Debug.Log(uPositiveFloat.Value);
    }
}
