﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityUtils.Variables;
using UnityUtils.Variables.Floats;

public class URangeFloatTest : MonoBehaviour
{
    //if (object is [1,2] range, this should show warning icon (use this restriction)
    [FloatRange(1.1f, 1.9f)]
    [Variable(typeof(RangeFloat))]
    public UFloat uRangeFloat;
    
    //if (object is [1,2] range, this should show error icon (use this restriction)
    [FloatRange(0.9f, 2.1f)]
    [Variable(typeof(RangeFloat))]
    public UFloat uRangeFloat2;
    
    //no range restriction (use objects)
    [Variable(typeof(RangeFloat))]
    public UFloat uRangeFloat3;
}
