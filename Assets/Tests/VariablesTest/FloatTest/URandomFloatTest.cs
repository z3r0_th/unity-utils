﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityUtils.Variables;
using UnityUtils.Variables.Floats;

public class URandomFloatTest : MonoBehaviour
{
    //if (object is [1,2] range, this should show warning icon (use this restriction)
    [FloatRange(1.1f, 1.9f)]
    [Variable(typeof(RandomFloat))]
    public UFloat uRandomFloat;
    
    //if (object is [1,2] range, this should show error icon (use this restriction)
    [FloatRange(0.9f, 2.1f)]
    [Variable(typeof(RandomFloat))]
    public UFloat uRandomFloat2;
    
    //no range restriction (use objects)
    [Variable(typeof(RandomFloat))]
    public UFloat uRandomFloat3;
}
