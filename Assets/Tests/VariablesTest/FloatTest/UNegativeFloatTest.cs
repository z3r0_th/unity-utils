﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityUtils.Variables;
using UnityUtils.Variables.Floats;

public class UNegativeFloatTest : MonoBehaviour
{
    [Variable(typeof(NegativeFloat))]
    public UFloat uNegativeFloat;
}
