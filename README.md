# How to use in Unity Package

open ```manifest.json``` located at Packages folder in your unity project ```Project/Packages/manifest.json```

add the following line in the manifest dependencies:

```"com.unityutils": "https://gitlab.com/z3r0_th/unity-utils.git#upm"```

it should look similar to (with a bunch more lines):

```json
{
  "dependencies": {
    "com.unityutils": "https://gitlab.com/z3r0_th/unity-utils.git#upm",
    "com.unity.2d.animation": "3.1.1",
    "com.unity.2d.pixel-perfect": "2.0.4"
  }
}
```